package main

import (
	//"fmt"
	"math"
)

type RageBot struct {
	car               CarId
	race              Race
	lastPiecePosition PiecePosition
	lastSpeed         float64
	lastAngle         float64
	lastAngleSpeed    float64
	crashed           bool
	accelConst        float64
	dragConst         float64
	throttle          float64
	shifted           bool
	turbo             bool
	turboTicks        int
	turboFactor       float64
	accelMult         float64
}

func (self *RageBot) GameInit(data GameInit) {
	self.race = data.Race
}

func (self *RageBot) MyCar(car CarId) {
	self.car = car
}

func (self *RageBot) RaceStart() Sendable {
	//fmt.Println("START")
	return Throttle(1.0)
}

func (self *RageBot) TurboAvailable(turboFactor float64, ticks int) {
	self.turbo = true
	self.turboTicks = ticks
	self.turboFactor = turboFactor
}

func (self *RageBot) Step(cars CarPositions, tick int) Sendable {
	if self.crashed {
		return Ping{}
	}

	var mycar CarPosition
	for _, cp := range cars {
		if cp.Id == self.car {
			mycar = cp
		}
	}

	track := self.race.Track
	piecePosition := mycar.PiecePosition
	piece := track.Pieces[piecePosition.PieceIndex]

	speed := track.Dist(self.lastPiecePosition, piecePosition)
	accel := speed - self.lastSpeed
	var trackAngleSpeed float64 = 0
	if piece.Radius > 0 {
		trackAngleSpeed = speed / track.Radius(piecePosition)
		trackAngleSpeed = math.Copysign(trackAngleSpeed, piece.Angle)
	}

	angle := mycar.Angle * math.Pi / 180.0
	angleSpeed := angle - self.lastAngle + trackAngleSpeed
	//angleAccel := angleSpeed - self.lastAngleSpeed

	var acp float64
	if piece.Radius > 0 {
		acp = speed * speed / track.Radius(piecePosition)
		acp = math.Copysign(acp, piece.Angle)
	}

	/*var centripetalConst float64 = (angleAccel + 0.00875 * self.lastAngle + 0.1 * self.lastAngleSpeed)
	if (piece.Radius > 0) {
		centripetalConst = centripetalConst/acp/math.Cos(angle)
	}*/

	defer func() {
		/*fmt.Printf("Angle: %-20g\tAngle': %-20g\tAngle'': %-20g\n", angle, angleSpeed, angleAccel)
		fmt.Printf("Speed: %-20g\tAccel: %-20g\tAcp: %-20g\n", speed, accel, acp)
		fmt.Printf("Throttle: %-20g\tBend: %-20g\tRadius: %-20g\n", self.throttle, piece.Angle, track.Radius(piecePosition))
		fmt.Printf("AccelConst: %-20g\tDragConst: %-20g\n", self.accelConst, self.dragConst)
		fmt.Println("------------------------------------------------------------------------------")*/
		self.lastPiecePosition = piecePosition
		self.lastAngle = angle
		self.lastAngleSpeed = angleSpeed
		self.lastSpeed = speed
	}()

	if tick == 0 {
		self.crashed = false
		self.lastPiecePosition = PiecePosition{}
		self.lastAngle = 0
		self.lastAngleSpeed = 0
		self.lastSpeed = 0
		self.accelConst = 0
		self.dragConst = 0
		self.throttle = 1
		self.shifted = false
		self.turbo = false
		self.turboTicks = 0
		self.turboFactor = 1
		self.accelMult = 1
		return Throttle(1.0)
	}

	if tick == 1 {
		self.accelConst = accel
		return Throttle(1.0)
	}

	if tick == 2 {
		self.dragConst = (accel - self.accelConst) / self.lastSpeed
		return Throttle(1.0)
	}

	targetAcp := float64(0.44)

	if self.accelMult != 1 {
		self.turboTicks -= 1
	}

	if self.turboTicks == 0 {
		self.accelMult = 1
	}

	if self.turbo {
		if piece.Radius == 0 && piecePosition.InPieceDistance < 15 {
			self.accelMult = self.turboFactor
			self.turbo = false
			//fmt.Println("TURBOOOOOOOOOOOOOO")
			return Turbo("I'm in a hurry!")
		}
	}

	var targetspeed float64 = 13

	if piece.Radius > 0 {
		targetspeed = math.Sqrt(targetAcp * track.Radius(piecePosition))
	}

	nextpiece := track.Pieces[(piecePosition.PieceIndex+1)%len(track.Pieces)]

	nextPP := piecePosition
	nextPP.Lane.StartLaneIndex = piecePosition.Lane.EndLaneIndex
	nextPP.PieceIndex = (nextPP.PieceIndex + 1) % len(track.Pieces)
	nextPP.InPieceDistance = 0.0

	if piecePosition.PieceIndex != self.lastPiecePosition.PieceIndex {
		self.shifted = false
	}

	if nextpiece.Switch && !self.shifted {
		numlanes := len(track.Lanes)
		tracklens := make([]float64, numlanes)
		pp := nextPP
		pp.PieceIndex = (pp.PieceIndex + 1) % len(track.Pieces)
		for ; track.Pieces[pp.PieceIndex].Switch == false; pp.PieceIndex = (pp.PieceIndex + 1) % len(track.Pieces) {
			if track.Pieces[pp.PieceIndex].Radius > 0 {
				for lane := 0; lane < numlanes; lane++ {
					pp.Lane.StartLaneIndex = lane
					pp.Lane.EndLaneIndex = lane
					tracklens[lane] += track.PieceLength(pp)
				}
			}
		}

		if piecePosition.Lane.EndLaneIndex > 0 && tracklens[piecePosition.Lane.EndLaneIndex-1] < tracklens[piecePosition.Lane.EndLaneIndex] {
			self.shifted = true
			return SwitchLane("Left")
		}

		if piecePosition.Lane.EndLaneIndex < numlanes-1 && tracklens[piecePosition.Lane.EndLaneIndex+1] < tracklens[piecePosition.Lane.EndLaneIndex] {
			self.shifted = true
			return SwitchLane("Right")
		}
	}

	if nextpiece.Radius > 0 {
		var ts float64 = math.Sqrt(targetAcp * track.Radius(nextPP))

		if ts < speed {
			nextspeed := speed
			dst := float64(0.0)
			for nextspeed > ts+0.5 {
				dst += nextspeed
				nextspeed += self.dragConst * nextspeed
			}
			if dst > (track.PieceLength(piecePosition) - piecePosition.InPieceDistance) {
				targetspeed = ts
			}
		}
	}

	nextnextpiece := track.Pieces[(piecePosition.PieceIndex+2)%len(track.Pieces)]

	if nextnextpiece.Radius > 0 {
		nextnextPP := nextPP
		nextnextPP.PieceIndex = (nextPP.PieceIndex + 1) % len(track.Pieces)
		var ts float64 = math.Sqrt(targetAcp * track.Radius(nextnextPP))

		if ts < speed {
			nextspeed := speed
			dst := float64(0.0)
			for nextspeed > ts+0.5 {
				dst += nextspeed
				nextspeed += self.dragConst * nextspeed
			}
			if dst > (track.PieceLength(piecePosition) + track.PieceLength(nextPP) - piecePosition.InPieceDistance) {
				targetspeed = ts
			}
		}
	}

	self.throttle = (targetspeed - speed - self.dragConst*speed) / (self.accelConst * self.accelMult)
	self.throttle = math.Max(math.Min(self.throttle, 1), 0)
	return Throttle(self.throttle)
}

func (self *RageBot) Crashed(car CarId) {
	//fmt.Println(car, "crashed")
	if car == self.car {
		self.crashed = true
	}
}

func (self *RageBot) Spawned(car CarId) {
	//fmt.Println(car, "spawned")
	if car == self.car {
		self.crashed = false
	}
}
