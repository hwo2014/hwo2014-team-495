package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"math"
	"net"
)

type Bot interface {
	GameInit(data GameInit)
	MyCar(car CarId)
	RaceStart() Sendable
	Step(cars CarPositions, tick int) Sendable
	Crashed(car CarId)
	Spawned(car CarId)
	TurboAvailable(turboFactor float64, ticks int)
}

type Sendable interface {
	ToMsg() Msg
}

type Msg struct {
	MsgType  string      `json:"msgType"`
	Data     interface{} `json:"data,omitempty"`
	GameTick int         `json:"gameTick,omitempty"`
}

type BotId struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}

type CarId struct {
	Name  string
	Color string
}

type Join struct {
	BotId
}

func (self Join) ToMsg() Msg {
	return Msg{"join", self, 0}
}

type JoinRace struct {
	BotId     BotId  `json:"botId"`
	TrackName string `json:"trackName"`
	Password  string `json:"password"`
	CarCount  int    `json:"carCount"`
}

func (self JoinRace) ToMsg() Msg {
	return Msg{"joinRace", self, 0}
}

type YourCar struct {
	CarId
}

type Piece struct {
	Length float64
	Switch bool
	Radius float64
	Angle  float64
}

type Lane struct {
	DistanceFromCenter float64
	Index              int
}

type StartingPoint struct {
	Position struct {
		X float64
		Y float64
	}
	Angle float64
}

type Car struct {
	Id         CarId
	Dimensions struct {
		Length            float64
		Width             float64
		GuideFlagPosition float64
	}
}

type RaceSession struct {
	Laps         int
	MaxLapTimeMs float64
	QuickRace    bool
	DurationMs   float64
}

type Track struct {
	Id            string
	Name          string
	Pieces        []Piece
	Lanes         []Lane
	StartingPoint StartingPoint
}

func (self *Track) PieceLength(pp PiecePosition) float64 {
	piece := self.Pieces[pp.PieceIndex]
	if piece.Angle == 0 {
		return piece.Length
	}

	r := self.Radius(pp)

	return math.Abs(piece.Angle) * math.Pi / 180 * r
}

func (self *Track) Radius(pp PiecePosition) float64 {
	piece := self.Pieces[pp.PieceIndex]
	if piece.Angle == 0 {
		return 0
	}

	r := piece.Radius
	laned := (self.Lanes[pp.Lane.StartLaneIndex].DistanceFromCenter + self.Lanes[pp.Lane.EndLaneIndex].DistanceFromCenter) / 2
	if piece.Angle > 0 {
		r -= laned
	} else {
		r += laned
	}

	return r
}

func (self *Track) Dist(pp1, pp2 PiecePosition) float64 {
	var speed float64
	if pp1.PieceIndex == pp2.PieceIndex {
		speed = pp2.InPieceDistance - pp1.InPieceDistance
	} else {
		speed = pp2.InPieceDistance + (self.PieceLength(pp1) - pp1.InPieceDistance)
	}

	return speed
}

type Race struct {
	Track       Track
	Cars        []Car
	RaceSession RaceSession
}

type GameInit struct {
	Race Race
}

type PiecePosition struct {
	PieceIndex      int
	InPieceDistance float64
	Lane            struct {
		StartLaneIndex int
		EndLaneIndex   int
	}
	Lap int
}

type CarPosition struct {
	Id            CarId
	Angle         float64
	PiecePosition PiecePosition
}

type CarPositions []CarPosition

type GameStart struct{}

type Throttle float64

func (self Throttle) ToMsg() Msg {
	return Msg{"throttle", self, 0}
}

type Ping struct{}

func (self Ping) ToMsg() Msg {
	return Msg{"ping", nil, 0}
}

type Crash struct {
	CarId
}

type Spawn struct {
	CarId
}

type SwitchLane string

func (self SwitchLane) ToMsg() Msg {
	return Msg{"switchLane", self, 0}
}

type Turbo string

func (self Turbo) ToMsg() Msg {
	return Msg{"turbo", self, 0}
}

type TurboAvailable struct {
	TurboDurationMilliseconds float64
	TurboDurationTicks        int
	TurboFactor               float64
}

type Client struct {
	host     string
	port     int
	name     string
	key      string
	bot      Bot
	gametick int
	conn     *bufio.ReadWriter
}

func NewClient(host string, port int, name string, key string, bot Bot) Client {
	return Client{host, port, name, key, bot, 0, nil}
}

func (self *Client) Run() error {
	conn, err := net.Dial("tcp", fmt.Sprintf("%v:%v", self.host, self.port))
	if err != nil {
		return err
	}
	defer conn.Close()

	self.conn = bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))

	err = self.Send(Join{BotId{self.name, self.key}})
	if err != nil {
		return err
	}

	for {
		data, err := self.Receive()
		if err != nil {
			return err
		}
		switch data.(type) {
		case *GameInit:
			self.bot.GameInit(*data.(*GameInit))
		case *YourCar:
			self.bot.MyCar(data.(*YourCar).CarId)
		case *CarPositions:
			self.Send(self.bot.Step(*data.(*CarPositions), self.gametick))
		case *GameStart:
			self.Send(self.bot.RaceStart())
		case *Crash:
			self.bot.Crashed(data.(*Crash).CarId)
		case *Spawn:
			self.bot.Spawned(data.(*Spawn).CarId)
		case *TurboAvailable:
			self.bot.TurboAvailable(data.(*TurboAvailable).TurboFactor, data.(*TurboAvailable).TurboDurationTicks)
		}
	}
}

func (self *Client) Send(m Sendable) error {
	msg := m.ToMsg()
	//msg.GameTick = self.gametick
	return self.SendMsg(msg)
}

func (self *Client) SendMsg(msg Msg) error {
	line, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	//log.Println(string(line))

	_, err = self.conn.Write(line)
	if err != nil {
		return err
	}

	_, err = self.conn.WriteRune('\n')
	if err != nil {
		return err
	}

	err = self.conn.Flush()

	return err
}

func (self *Client) Receive() (data interface{}, err error) {
	line, _, err := self.conn.ReadLine()
	if err != nil {
		return
	}

	//log.Println(string(line))

	var msg Msg
	var d json.RawMessage
	msg.Data = &d
	err = json.Unmarshal(line, &msg)
	if err != nil {
		return
	}

	if msg.MsgType == "gameStart" {
		self.gametick = 0
	}

	if msg.GameTick > 0 {
		self.gametick = msg.GameTick
	}

	switch msg.MsgType {
	case "yourCar":
		data = new(YourCar)
	case "gameInit":
		data = new(GameInit)
	case "carPositions":
		data = new(CarPositions)
	case "gameStart":
		data = new(GameStart)
	case "crash":
		data = new(Crash)
	case "spawn":
		data = new(Spawn)
	case "turboAvailable":
		data = new(TurboAvailable)
	}

	if len(d) > 0 {
		err = json.Unmarshal([]byte(d), &data)
	}

	return
}
