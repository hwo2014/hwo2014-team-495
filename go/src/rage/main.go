package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func parseArgs() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func main() {

	host, port, name, key, err := parseArgs()

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)
	bot := new(RageBot)
	cli := NewClient(host, port, name, key, bot)
	err = cli.Run()
	if err != nil && err != io.EOF {
		log.Fatal(err)
		os.Exit(1)
	}
}
